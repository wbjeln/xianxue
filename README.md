# xianxue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
1.参赛队伍：7个学院共7支队伍，每个队伍3名参赛选手
2.题目类型：必答题（35题）、抢答题（20题）、风险题（10分、20分、30分题各14题，一共42题，每支参赛队伍需任意选择2题回答）
