export const bida = [
    {
        title:"这是标题标题这是标题标题这是标题标题这是标题标题1",
        right: ["a"],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"},
            e:{title:"选项e"}
        }
    },
    {
        title:"这是标题标题这是标题标题这是标题标题这是标题标题2(多选)",
        right: ['a','b'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
]
export const qiangda = [
    {
        title:"抢答这是标题标题这是标题标题这是标题标题这是标题标题1",
        right: ["a"],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"},
            e:{title:"选项e"}
        }
    },
    {
        title:"抢答这是标题标题这是标题标题这是标题标题这是标题标题2(多选)",
        right: ['a','b'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
    {
        title:"抢答这是标题标题这是标题标题这是标题标题这是标题标题3(多选)",
        right: ['a','b','c'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
]
export const fengxian1 = [
    {
        title:"10风险这是标题标题这是标题标题这是标题标题这是标题标题1",
        right: ["a"],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"},
            e:{title:"选项e"}
        }
    },
    {
        title:"10风险这是标题标题这是标题标题这是标题标题这是标题标题2(多选)",
        right: ['a','b'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
    {
        title:"10风险这是标题标题这是标题标题这是标题标题这是标题标题3",
        right: ["a"],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"},
            e:{title:"选项e"}
        }
    },
    {
        title:"10风险这是标题标题这是标题标题这是标题标题这是标题标题4(多选)",
        right: ['a','b'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
]
export const fengxian2 = [
    {
        title:"20风险这是标题标题这是标题标题这是标题标题这是标题标题1",
        right: ["a"],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"},
            e:{title:"选项e"}
        }
    },
    {
        title:"20风险这是标题标题这是标题标题这是标题标题这是标题标题2(多选)",
        right: ['a','b'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
    {
        title:"20风险这是标题标题这是标题标题这是标题标题这是标题标题3",
        right: ["a"],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"},
            e:{title:"选项e"}
        }
    },
    {
        title:"20风险这是标题标题这是标题标题这是标题标题这是标题标题4(多选)",
        right: ['a','b'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
]
export const fengxian3 = [
    {
        title:"30风险这是标题标题这是标题标题这是标题标题这是标题标题1",
        right: ["a"],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"},
            e:{title:"选项e"}
        }
    },
    {
        title:"30风险这是标题标题这是标题标题这是标题标题这是标题标题2(多选)",
        right: ['a','b'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
    {
        title:"30风险这是标题标题这是标题标题这是标题标题这是标题标题3",
        right: ["a"],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"},
            e:{title:"选项e"}
        }
    },
    {
        title:"30风险这是标题标题这是标题标题这是标题标题这是标题标题4(多选)",
        right: ['a','b'],
        choose:{
            a:{title:"选项A"},
            b:{title:"选项B"},
            c:{title:"选项C"},
            d:{title:"选项D"}
        }
    },
]